package com.hmtmcse.ocb
class MemberInterceptor {

    AuthenticationService authenticationService

    boolean before() {
        if (authenticationService.isAdministratorMember()){
            return true
        }
        flash.message = AppUtil.infoMessage("Sua conta não possui autorização para este local", false)
        redirect(controller: "dashboard", action: "index")
        return false
    }
}