<div class="form-group">
    <label><g:message code="first.name"/> *</label>
    <g:textField name="firstName" class="form-control" value="${member?.firstName}" placeholder="Please Enter First Name"/>

     <UIHelper:renderErrorMessage fieldName="firstName" model="${member}" errorMessage="please.enter.first.name"/>
</div>

<div class="form-group">
    <label><g:message code="last.name"/></label>
    <g:textField name="lastName" class="form-control" value="${member?.lastName}" placeholder="Please Last Name"/>
</div>

<div class="form-group">
    <label><g:message code="email.address"/> *</label> 
    <g:field type="email" name="email" class="form-control" value="${member?.email}" placeholder="Please Enter Email"/>

     <UIHelper:renderErrorMessage fieldName="email" model="${member}" errorMessage="Seu Email é Inválido ou não existe no sistema"/>
</div>

<g:if test="${!edit}">
    <div class="form-group">
        <label><g:message code="password"/> *</label>
        <g:passwordField name="password" class="form-control" value="${member?.password}" placeholder="Please Enter Password"/>

        <UIHelper:renderErrorMessage fieldName="password" model="${member}" errorMessage="Por Favor Inserir uma senha"/>
    </div>
</g:if>
